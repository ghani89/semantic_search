package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import recherchepack.Reponse;
import recherchepack.Resultat;


public class Fenetre extends JFrame {

	private JTextField champs_recherche;
	private JButton    bouton_recherche;
	private JPanel barre_recherche;
	private JPanel aff_resultat ;
	private JPanel aff_suggestion;
	


	public Fenetre(String title) throws HeadlessException {
		super(title);
		setSize(java.awt.Toolkit.getDefaultToolkit().getScreenSize());
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
	    barre_recherche = new JPanel();
		barre_recherche.setLayout(new FlowLayout());
		barre_recherche.setBackground(Color.GRAY);
		champs_recherche = new JTextField(40);
		bouton_recherche = new JButton("Rechercher");
		bouton_recherche.setBackground(Color.lightGray);
		bouton_recherche.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				rechercher(champs_recherche.getText());
			}});
		barre_recherche.add(champs_recherche);
		barre_recherche.add(bouton_recherche);
		add(barre_recherche, BorderLayout.NORTH);
		
		JPanel content = new JPanel();
		content.setLayout(new GridLayout(2, 1));
		
		aff_resultat = new JPanel();
		aff_resultat.setLayout(new FlowLayout());
		JScrollPane scrollPane = new JScrollPane(aff_resultat);
		content.add(scrollPane);
		aff_suggestion = new JPanel();
		aff_suggestion.setLayout(new FlowLayout());
		content.add(aff_suggestion);
		add(content,BorderLayout.CENTER);
		
		
		
	}

	public void rechercher(String textRequette)
	{
		aff_resultat.removeAll();
		aff_suggestion.removeAll();
		
		this.repaint();
		
		recherchepack.Recherche recherche = new recherchepack.Recherche();
		Resultat res = recherche.rechercher(textRequette);
		if(res==null)
			return;
		ArrayList<Reponse> reponses = res.getReponses();
		if(reponses !=null)
		{	
			if(reponses.size()==0)
				aff_resultat.add(new FReponse("Attention : ","Pas de résultat pour cette requette"));
			else
				for(int i = 0 ;i<reponses.size();i++  )
				{
					Reponse reponse = reponses.get(i);
			
			
					//aff_resultat.add(new FReponse("Titre",reponse.getProprietes().get("Titre")));
			
					//reponse.getProprietes().remove("Titre");
					for(String prop : reponse.getProprietes().keySet())
					{
				
						aff_resultat.add(new FReponse(prop, reponse.getProprietes().get(prop)));
						
					}
			
			
				}	
			
			
				
				
		}
		

		ArrayList<String> sugg = res.getVoisins();
		if(sugg!=null)
		{	aff_suggestion.setBorder(new TitledBorder(null, "Voir aussi", TitledBorder.CENTER, TitledBorder.BELOW_TOP, null, new Color(0, 0, 128)));
			for(int i = 0 ; i<sugg.size();i++)
			{
				aff_suggestion.add(new FVoisin( sugg.get(i),this));
			}
		}
		else
			aff_suggestion.setBorder(null);
		validate();
		
	}
	
	
}
