package gui;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FVoisin extends JPanel implements MouseListener {

	private Fenetre parent;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String instance; 
	
	


	public FVoisin( String valeur, Fenetre parent) {
		addMouseListener(this);
		//setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
		setLayout(new GridLayout(3, 1));
		
		setBackground(Color.blue);
		
		JLabel lvaleur = new JLabel(valeur);
		lvaleur.setForeground(Color.black);
		add(new JLabel(""));
		add(lvaleur);
		
		add(new JLabel(""));
		
		
		instance = valeur;
		this.parent = parent;
	}
	
	public void mouseClicked(MouseEvent e) {
		
		parent.rechercher(instance);
		
		
	}


	public void mouseEntered(MouseEvent e) {
		setBorder(BorderFactory.createMatteBorder(4, 4, 4, 4, Color.blue));
		
	}


	public void mouseExited(MouseEvent e) {
	//	setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
		setBorder(null);
	}


	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
