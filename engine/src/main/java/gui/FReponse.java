package gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class FReponse extends JPanel {

	
	
	public FReponse(String prop , String valeur) {
		super();
		
		//setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.RED));
		//setMaximumSize(new Dimension(150,150));
		setLayout(new GridLayout(2, 1));
		JLabel lprop = new JLabel(prop);
		
		lprop.setForeground(Color.BLUE);
		 
		
		
			JTextArea lvaleur = new JTextArea(5, 20);
			lvaleur.setBorder(null);
			lvaleur.setBackground(this.getBackground());
			
			lvaleur.setEditable(false);
			lvaleur.append(valeur);
			lvaleur.setCaretPosition(lvaleur.getDocument().getLength());
			lvaleur.setFont(new Font("Serif", Font.ITALIC, 16));
			lvaleur.setLineWrap(true);
			lvaleur.setWrapStyleWord(true);
			JScrollPane scrollPane = new JScrollPane(lvaleur);
			lvaleur.setForeground(Color.black);
			
		
		
		
		add(lprop);
		if(prop.equals("Photo"))
		{
			ImagePanel p =new ImagePanel(createImageIcon(valeur));
			add(p);
			JFrame f = new JFrame();
			f.setSize(128, 128);
			f.setContentPane(p);
			f.setVisible(true);
		}
		else
			add(scrollPane);
		
		
	}


	protected Image createImageIcon(String path) {
		

		Image image = null;
		try {
			URL url = new URL(path);
			image = ImageIO.read(url);
		} catch (IOException e) {
		}
		
		return image;
		
	}
	

}
