package recherchepack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import parser.ParserFactory;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntProperty;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import com.hp.hpl.jena.rdf.model.Property;
public class Recherche {
	
	public  Ontologie ontologie ;
	public  ArrayList<Individual> instances;
	public ArrayList<String> classes;
	private ExtendedIterator it = null; 
	private String nomInstance;
	
	public Recherche() {
		super();
		 ontologie = new Ontologie("uvsq.owl");
		 instances = ontologie.getInstances();
		 classes = ontologie.listeClasse();
		
		 
	}
	
	
	private  ArrayList<String> decomposerStr(String mot) 
	{
		
		String fichier = "Map.xml";
		
		ArrayList<String> motcles = new ArrayList<String>();
		
		String requete=mot.toLowerCase();
		
			
			MapXML mapXML= new MapXML(fichier);
			
			
			Map<String, String> map =mapXML.getSynonyme_map();
			
			
			 for(String synonyme: map.keySet())			 
				 if(requete.contains(synonyme))
				 {
					 motcles.add(map.get(synonyme));
					 requete=requete.replaceAll(synonyme, ""); 
				  }
			 
			 StringTokenizer st = new StringTokenizer(requete);
			 while (st.hasMoreTokens()) {
			 	 motcles.add(st.nextToken());
				}
			 
		    if (motcles.size()==0)
		    	 return null;
		    else
		    	
		        return motcles;
		}
	
	public  boolean isInstanceName(String nom)
	{
		
		 for(Individual instance : instances)
		 {
			if( instance.getLocalName().equals(nom))
			{
				return true ;
			}
		 }
		
		
		return false;
	}
	public boolean isClasses(String mot)
	{
		for(String c : classes)
		{
			if(c.equals(mot))
				return true;
		}
		return false;
	}
	
	public  Resultat rechercher(String req)
	{
		
		
		
		ArrayList<String> insts = new ArrayList<String>(); //les instances dans la requette
		ArrayList<String> cls = new ArrayList<String>(); // les classes dans la requette
		ArrayList<String> props = new ArrayList<String>();//les propriétés dans la requette
		
		boolean inst = false; // variable qui teste si la requette ne contient que d'instances
		ArrayList<String> mots = decomposerStr(req);
		
		//requette non reconnu 
		if(mots==null)
		{
			Reponse r = new Reponse("information");
			r.ajouterProprieteValeur("Information :", "Pas de résultats pour cette requette");
			ArrayList<Reponse> reponses = new ArrayList<Reponse>();
			reponses.add(r);
			return new Resultat(null, null);
		}
		
		//identification des mots de la requette		
		for(String m : mots)
		{
			if(isInstanceName(m))
				insts.add(m);
			else
			if(isClasses(m))
			   cls.add(m);
			else
				props.add(m);
			
		}
		
		//on peut traiter (instance et propriétés , instances , classe et instance ,instance et classe )
		
		ArrayList<String> requette = new ArrayList<String>();
		
		//cas 1: une instance et propriétés  ou une instance =============================================================================
		//on cherche des informations sur l'instance (ses propriétés)
		if(insts.size()==1 && cls.size()==0)
		{
		
			String ins = insts.get(0);
			requette.add(ins);
			nomInstance = ins;
			
			for(Individual instance : instances)
			{
				if(instance.getLocalName().equals(ins))
				{
				
					Iterator<OntProperty> prps=  instance.getOntClass().listDeclaredProperties();
					 it = instance.getOntClass().listInstances();
					StmtIterator propsinst = instance.listProperties(); //propriétés de l'instance 
					 
										 
					if( props.size() > 0 )
					{
						//comparer les propriétés de la requette a celles de la classe de l'instance
						for(int i = 0 ; i < props.size() ; i++)
						{
						    prps=  instance.getOntClass().listDeclaredProperties();
							while(prps.hasNext())
							{
								OntProperty prop = prps.next();
								if(prop.getLocalName().equals(props.get(i)))
								{
									
									requette.add(prop.getLocalName());
									break;	
								}	
								
							}
							
							//comparer les propriétés de la requette a celles  de l'instance
							 propsinst = instance.listProperties();
							 while(propsinst.hasNext())
							 {
								 Statement stmt = propsinst.nextStatement();
								 Property  prop = stmt.getPredicate();
								 if(prop.getLocalName().equals(props.get(i)))
								 {
							
									 requette.add(prop.getLocalName());
									 break;	
								 }
								 
							 }

						}
						
					}else
					{
							prps=  instance.getOntClass().listDeclaredProperties();
							while(prps.hasNext())
							{
								OntProperty prop = prps.next();
								requette.add(prop.getLocalName());
						
							}
							
							 propsinst = instance.listProperties();
							 while(propsinst.hasNext())
							 {
								 Statement stmt = propsinst.nextStatement();
								 Property  prop = stmt.getPredicate();
								 requette.add(prop.getLocalName());
								 
							 }
					}
					
					
					break;
				}
			
				 
			 }
		
			//===========================================passer la requette au parseur=====================
			parser.ParserFactory factory = null;
			try {
				factory = new ParserFactory(requette.get(0));
			} catch (ClassNotFoundException e) {
				
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				
				e.printStackTrace();
			} catch (SAXException e) {
				
				e.printStackTrace();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
			if(factory == null )
				return null;
			
			ArrayList<Reponse> reponses = factory.getResponse(requette);
			
			
			return new Resultat(reponses, getVoisins());
			
			
		}
		
		//======================================= fin de cas1 ==========================================================================
		
		
		
		
		
		
		
		//cas2: que des instances
		//on affiche la liste des instances cliquables 
		if( insts.size()>0 && cls.size() == 0 && props.size()==0 )
		{
			ArrayList<String> voisins = new ArrayList<String>();
			
			String fichier = "Map.xml";
			MapXML mapXML= new MapXML(fichier);
			
			
			Map<String, String> map =mapXML.getSyno_invers_map();
			
			
			for(String ins:insts)
			{
				System.out.println(ins);
				voisins.add(map.get(ins));
			}
			
			return new Resultat(null, voisins);
			
		}
		//============== fin de cas 2 ================================================================================================
		
				
		
		
		
		//cas3 : (une classe => une instance) ou (une instance => une classe)
		//recherche des instances de la classe ayant relation avec l'instance
		if( insts.size()==1 && cls.size()==1 && props.size()==0 )
		{
			String classe = cls.get(0);
			String instance = insts.get(0);
			Individual ind_instance = null; //notre instance 
			
			//voir si instance est une instance de classe
			boolean isinstclasse = false;
			
			for(Individual ins : instances)
			{
				if(ins.getLocalName().equals(instance))
				{
					ind_instance = ins;
					ExtendedIterator instsclasse = ontologie.getClasse(classe).listInstances();
					it = instsclasse;
				
					while(instsclasse.hasNext())
					{
						Individual instclasse = (Individual) instsclasse.next();
						if(instclasse.getLocalName().equals(instance))
						{
							isinstclasse = true;
							break;
						}
						
						
					}
					
					break;
				}
			}
				
			if(isinstclasse) 
			{	
				
				// on renvoie les information consernants l'instance 
				requette.add(instance);
				
				//===========================================passer la requette au parseur=====================
				parser.ParserFactory factory = null;
				try {
					factory = new ParserFactory(instance);
				} catch (ClassNotFoundException e) {
					
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					
					e.printStackTrace();
				} catch (SAXException e) {
					
					e.printStackTrace();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				
				if(factory == null )
					return null;
				
				ArrayList<Reponse> reponses = factory.getResponse(requette);
				
				
				return new Resultat(reponses, getVoisins());
				
			}
			it = ontologie.getClasse(classe).listInstances();
			//cas ou l'instance n'est pas de la classe
			ArrayList<String> instanceDeClasseRechercher=new ArrayList<String>();
			//parcourir toutes les instances de la classe 
			while( it.hasNext())
			{
				boolean trouve = false;
				//selectionner une instance de la classe
				Individual ind = (Individual) it.next();
				//voir si l'instance de la classe est differente de notre instance (juste pour precotion) 
				if(!ind.getLocalName().equals(instance))
				{
					//voir si une des propriétés de notre instance cible l'instance de la classe
					StmtIterator propsinst_inst = ind_instance.listProperties();
					while(propsinst_inst.hasNext())
					{
						Statement stmt = propsinst_inst.nextStatement();
						
						
						if((stmt.getObject()).toString().equals(ind.toString())) //si la proprieté cible l'instance
						{
							
							instanceDeClasseRechercher.add(ind.getLocalName()); // l'instance de la classe est trouvé 
							//System.out.println(instanceDeClasseRechercher);
							trouve = true;
							break;
						}
						
					}
					
					if(!trouve)
					{
					
					//voir si une propriété de l'instance de classe cible notre instance 
					StmtIterator propsinst = ind.listProperties();
					while(propsinst.hasNext())
					{
						Statement stmt = propsinst.nextStatement();
						
						if((stmt.getObject()).toString().equals(ind_instance.toString())) //si la proprieté cible l'instance
						{
							
							instanceDeClasseRechercher.add(ind.getLocalName()); // l'instance de la classe est trouvé 
							//System.out.println(instanceDeClasseRechercher);
							break;
						}
					}
					}
					
				}
			}
			
			if(instanceDeClasseRechercher.size()>0) // si on a trouvé l'instance de la classe conserné 
			{
				
				

				ArrayList<String> voisins =new ArrayList<String>();
				String fichier = "Map.xml";
				MapXML mapXML= new MapXML(fichier);
				Map<String, String> map =mapXML.getSyno_invers_map();
				
				for(String in : instanceDeClasseRechercher)
				{
					voisins.add(map.get(in));
				}
				
				return new Resultat(null, voisins);
			}
			
			
			
			
		}
		//===================== fin de cas3 ==========================================================================================
		
		//cas 4 : une classe 
		//on affiche la liste des instances de la classe cliquable
		if( cls.size()==1 && props.size()==0 && insts.size()==0 )
		{
			OntClass classe = ontologie.getClasse(cls.get(0));
			System.out.println(classe.getLocalName());
			ExtendedIterator individuals = classe.listInstances();
			ArrayList<String> voisins = new ArrayList<String>();
			String fichier = "Map.xml";
			MapXML mapXML= new MapXML(fichier);
			Map<String, String> map =mapXML.getSyno_invers_map();
			
			while(individuals.hasNext())
			{
				Individual ind = (Individual) individuals.next();
				
				voisins.add(map.get(ind.getLocalName()));
				
			}
			return new Resultat(null, voisins);
			
		}
		
		
		
		
		
		
		return null;
	}
	
	
	
	
	public ArrayList<String> getVoisins()
	{
		if(it == null)
			return null;
		
		ArrayList<String> voisins = new ArrayList<String>();
		
		String fichier = "Map.xml";
		MapXML mapXML= new MapXML(fichier);
		
		
		Map<String, String> map =mapXML.getSyno_invers_map();
		
		
		while(it.hasNext())
		{
			Individual ind = (Individual) it.next();
			if(!ind.getLocalName().equals(nomInstance))
			voisins.add(map.get(ind.getLocalName()));
		}
		
		return voisins;
	}


}
