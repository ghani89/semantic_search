package recherchepack;


import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;


public class Reponse {
	
	private String instance ; //l'entité a rechercher 
	private Map<String,String> proprietes ; //liste des propriétés et leurs valeurs correspondantes 
	
	public Reponse(String instance) {
		super();
		this.instance = instance;
		proprietes = newHashMap();
	}
	
	//donner une valeur a une propriété
	public void ajouterProprieteValeur(String prop , String valeur)
	{
		proprietes.put(prop, valeur);
	}

	public String getInstance() {
		return instance;
	}

	public Map<String, String> getProprietes() {
		return proprietes;
	}
	
	
	
	
}
