package recherchepack;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOError;
import java.util.ArrayList;
import java.util.Iterator;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.Ontology;
import com.hp.hpl.jena.ontology.impl.OntologyImpl;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class Ontologie {
	
	private OntModel model = null;
	private String uri;
	private ArrayList<Individual> instances;
	
	
	public Ontologie(String owlFile) {
		super();
		creerModel(owlFile);
		
	}
	
	private void creerModel(String owlFile)
	{
		FileReader ModelReader	=null ;
		
		try {
			ModelReader	= new FileReader(owlFile) ;
			model= ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
			model.read(ModelReader,null);
			
		}
		//Fichier Non trouvÃ©
		catch (FileNotFoundException e) {
			
			System.out.println("fichier non trouvé");
		}
		//Erreur de Fichier
		catch (IOError e) {
			System.out.println("erruer de fichier");
		}
		catch (Exception e ){

		}
		
		if(model != null)
		{
			Iterator<Ontology> iter = model.listOntologies();
			OntologyImpl ontimpl= (OntologyImpl) iter.next();
			uri = ontimpl.getURI().toString();
		
			chargerInstances();
		}	
		
	}
	
	private void chargerInstances()
	{
		instances = new ArrayList<Individual>();
		
		Iterator<Individual> individuals= model.listIndividuals();
		while(individuals.hasNext())
		{
			Individual instance	= individuals.next();
			if (instance.getURI().toString().equalsIgnoreCase(uri)==false)
			{
				if (instances.contains(instance)==false) 
				{
					instances.add(instance);
									
				}
			}
			
		}
		
	}
	
	public  ArrayList<String> listeClasse() {
		Iterator<OntClass> iter = model.listClasses();
		ArrayList<String> classesList = new ArrayList<String>();
		while (iter.hasNext()) {
			OntClass classes= (OntClass) iter.next();
			if (classes.getLocalName()!=null){
			if (classes.isRestriction()!=true){ //Il faut Enlever les Classes Restrictions
				classesList.add(classes.getLocalName());
				}
			}
		}
		
		return classesList;
	}


	public ArrayList<Individual> getInstances() {
		return instances;
	}
	
	public OntClass getClasse(String classe)
	{
		
		Iterator<OntClass> iter = model.listClasses();
		
		while (iter.hasNext()) {
			OntClass classes= (OntClass) iter.next();
			if (classes.getLocalName().equals(classe)){
				return classes;
			}
			
		}
		
		return null;
	
		
	}

}


