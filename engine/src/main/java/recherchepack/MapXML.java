package recherchepack;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class MapXML {
	
	
private String fichier;
private Map<String,String> synonyme_map;
private Map<String,String> syno_invers_map;

public MapXML(String fichier) 
{
	
		this.fichier = fichier;
		synonyme_map = new HashMap<String, String>();
		syno_invers_map = new HashMap<String, String>();
		
		String cle = null;
	    String valeur=null;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder build = null;
		
		try {
			build = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Document document = null;
		
		try {
			
			if(build != null)
				document = build.parse(fichier);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(document != null)
		{
			Element racine = document.getDocumentElement();
			NodeList bases =racine.getElementsByTagName("synonyme");
		
			for(int i=0; i<bases.getLength(); i++)
			{
				Node base=bases.item(i);
				//elements enfant
				NodeList element = base.getChildNodes();
			
				for(int j=0; j<element.getLength(); j++){
					Node enfant=element.item(j);
					if(enfant.getNodeName().equals("req-name"))
						cle=enfant.getTextContent();
					
				
					if(enfant.getNodeName().equals("rdf-name"))
					
						valeur=enfant.getTextContent();
					}
		
					synonyme_map.put(cle, valeur);
					if(!syno_invers_map.keySet().contains(valeur))
					{
						syno_invers_map.put(valeur, cle);
					}
			}

		}
		
}



	
	
	

	public Map<String, String> getSynonyme_map() {
		return synonyme_map;
	}


	public Map<String, String> getSyno_invers_map() {
		return syno_invers_map;
	}
	
	
	
}
