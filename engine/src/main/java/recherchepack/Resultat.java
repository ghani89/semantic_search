package recherchepack;

import java.util.ArrayList;

public class Resultat {
	private ArrayList<Reponse> reponses;
	private ArrayList<String> voisins;
	
	public Resultat(ArrayList<Reponse> reponses, ArrayList<String> voisins) 
	{
		super();
		this.reponses = reponses;
		this.voisins = voisins;
	}

	public ArrayList<Reponse> getReponses() {
		return reponses;
	}

	public void setReponses(ArrayList<Reponse> reponses) {
		this.reponses = reponses;
	}

	public ArrayList<String> getVoisins() {
		return voisins;
	}

	public void setVoisins(ArrayList<String> voisins) {
		this.voisins = voisins;
	}
	
	
	

}
