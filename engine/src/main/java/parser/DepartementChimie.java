package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import recherchepack.Reponse;

public class DepartementChimie implements Parser{
	
private Document doc;
	
	
	
	public DepartementChimie() {
		super();
		doc=null;
	}
	
	
	public Reponse parser(String url, ArrayList<String> requette) {


		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		reponse.ajouterProprieteValeur("Titre :",getTitreChimie());
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("direction"))
				{
					reponse.ajouterProprieteValeur("Direction :", getDirecteur());
				}
				else if(requette.get(i).equals("contact"))
				{
					reponse.ajouterProprieteValeur("Contact :",getContact());
				}
				else if(requette.get(i).equals("adresse"))
				{
					reponse.ajouterProprieteValeur("Adresse :",getAdresse());
				}
			}
		}	
		else 
		{
			reponse.ajouterProprieteValeur("Direction :", getDirecteur());
			reponse.ajouterProprieteValeur("Contact :",getContact());
			reponse.ajouterProprieteValeur("Adresse :",getAdresse());
			
		}
		
		
		
				
		
		
		return reponse;
	}
	
	
	public String  getTitreChimie()
	{
		
		return  doc.getElementsByTag("title").text();
	}
	public String  getDirecteur()
	{
		
		return doc.getElementsByTag("div").get(2).text();
		    	
	}
	public String getContact()
	{
	return doc.getElementsByTag("div").get(4).child(0).text();  


	}
	
	public String getAdresse()
	{
	return doc.getElementsByTag("div").get(3).text();

	}
	
	


}





