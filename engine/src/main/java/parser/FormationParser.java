package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recherchepack.Reponse;

public class FormationParser implements Parser{
	
	private Document doc;
	
	
	
	public FormationParser() {
		super();
		doc = null;
		
	}

	public Reponse parser(String url, ArrayList<String> requette) {
		
		
		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		reponse.ajouterProprieteValeur("Titre :",getTitreFormation());
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("langue"))
				{
					reponse.ajouterProprieteValeur("Langue : ",getInfGeneral("Langue(s) d'enseignement"));
				}
				else if(requette.get(i).equals("discipline"))
				{
					reponse.ajouterProprieteValeur("Discipline : ",getInfGeneral("Discipline(s)"));
				}else if(requette.get(i).equals("ville"))
				{
					reponse.ajouterProprieteValeur("Ville : ",getInfGeneral("Ville(s)"));
					
				}
				else if(requette.get(i).equals("description"))
				{
					reponse.ajouterProprieteValeur("Description : ",getDescription());
					
				}
				
				
			}
			
		}
		else
		{
			reponse.ajouterProprieteValeur("Langue : ",getInfGeneral("Langue(s) d'enseignement"));
			reponse.ajouterProprieteValeur("Discipline : ",getInfGeneral("Discipline(s)"));
			reponse.ajouterProprieteValeur("Ville : ",getInfGeneral("Ville(s)"));
			reponse.ajouterProprieteValeur("Description : ",getDescription());
			
		}
		
		return reponse;
	}
	
	private String  getTitreFormation()
	{
		
		return  doc.getElementById("contenu_avec_encadres_avec_nav").getElementsByTag("h1").get(0).text();
	}
	
	private String getInfGeneral(String inf)
	
	{
		Elements infos =doc.getElementById("details").getElementsByTag("tr");
		for(Element e:infos)
		{
			if(e.getElementsByTag("th").html().equalsIgnoreCase(inf))
			{
				return e.getElementsByTag("td").text();
			}
		}
		return "";
	}
	
	private String getDescription()
	{
		return doc.getElementById("contenu_onglet_presentation").getElementsByTag("div").get(0).text();
	}
	


}
