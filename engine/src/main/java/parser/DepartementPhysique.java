package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recherchepack.Reponse;

public class DepartementPhysique implements Parser{
private Document doc;
	
	
	
	public DepartementPhysique() {
		super();
		doc=null;
	}
	
	
	public Reponse parser(String url, ArrayList<String> requette) {


		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		reponse.ajouterProprieteValeur("Titre :",getTitrePhysique());
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("direction"))
				{
					reponse.ajouterProprieteValeur("direction :", getDirecteur());
				}
				else if(requette.get(i).equals("secretariat"))
				{
					reponse.ajouterProprieteValeur("secretariat :",getContact());
				}
				
			}
		}	
		else 
		{
			reponse.ajouterProprieteValeur("Directeur :", getDirecteur());
			reponse.ajouterProprieteValeur("Contact :",getContact());
			
		}
		
		
		
				
		
		
		return reponse;
	}
	
	
	public String  getTitrePhysique()
	{
		
		return  doc.getElementById("nom_entite_accroche").getElementsByTag("p").text();
	}
	public String  getDirecteur()
	{
		
		String s=doc.getElementsByTag("h2").get(0).text();
		String v=doc.getElementsByTag("h2").get(1).text();
        String result=s.concat("\n").concat(v);
	    return result;
		    	
	}
	public String getContact()
	{
		String s=doc.getElementsByClass("style2_deco").text();
		return s;
	}
	
	
	


}



