package parser;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlUrlParser {
	
	private HashMap<String,Class> map ;
	
	public XmlUrlParser(String file,String instance) throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException
	{
		map = new HashMap<String, Class>();

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document document = builder.parse(file);
		Element racine = document.getDocumentElement();
		NodeList bases = racine.getElementsByTagName(instance);
		
		for (int i = 0; i<bases.getLength();i++)
		{
			Node base = bases.item(i);
			
			NodeList parsers = base.getChildNodes();
			
			for (int j = 0 ; j < parsers.getLength() ; j++)
			{
				Node parser = parsers.item(j);
				NodeList enfants = parser.getChildNodes();
				String url =null;
				String classe=null;
				for(int k = 0 ; k < enfants.getLength() ; k++)
				{
					
					Node enfant = enfants.item(k);
					if(enfant.getNodeName().equals("url-name"))
					{
						url=enfant.getTextContent();
						System.out.println(url);
				
					}	
					if(enfant.getNodeName().equals("class-name"))
					{
						classe=enfant.getTextContent();
						System.out.println(classe);
				
					}
					
				}
				if(url !=null && classe!=null)
				 map.put(url,Class.forName(classe));
				
			}
			
		
		}

		
	}


	public HashMap<String, Class> getMap() {
		return map;
	}

	
}
