package parser;

import java.io.IOException;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recherchepack.Reponse;

public class ComposanteParser implements Parser{

	/**
	 * @param args
	 */
	
	private Document doc;
	
	
	
	public ComposanteParser() {
		super();
		doc=null;
	}
	
	
	public Reponse parser(String url, ArrayList<String> requette) {


		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		reponse.ajouterProprieteValeur("Titre :",getTitreComposante());
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("adresse"))
				{
					reponse.ajouterProprieteValeur("Adresse :", getInfGeneral("Adresse&nbsp;:"));
				}
				else if(requette.get(i).equals("tel_et_fax"))
				{
					reponse.ajouterProprieteValeur("Tel/Fax :",getInfGeneral("Tel/Fax&nbsp;:"));
				}
				else if(requette.get(i).equals("contacts"))
				{
					reponse.ajouterProprieteValeur("Contacts :",getContact());
				}
				else if(requette.get(i).equals("présentation"))
				{
					reponse.ajouterProprieteValeur("Presentation :",getPresentation());
				}
			}
		}	
		else 
		{
			reponse.ajouterProprieteValeur("Adresse :", getInfGeneral("Adresse&nbsp;:"));
			reponse.ajouterProprieteValeur("Tel/Fax :",getInfGeneral("Tel/Fax&nbsp;:"));
			reponse.ajouterProprieteValeur("Contacts :",getContact());
			reponse.ajouterProprieteValeur("Presentation :",getPresentation());
			
		}
		
		
		
				
		
		
		return reponse;
	}
	
	
	public String  getTitreComposante()
	{
		
		return  doc.getElementById("contenu_avec_encadres_avec_nav").getElementsByTag("h1").get(0).text();
	}
	public String  getContact()
	{
		
		Element s=doc.getElementsByClass("encadre_fiche").get(3);
		StringBuffer resultat= new StringBuffer();		
	    Element e =s.getElementsByTag("a").get(2);
	    String v = s.text();
	    resultat.append (v.replaceAll(e.text(),""));
	    String r=resultat.toString();
	    return r;
		    	
	}
	public String getInfGeneral(String inf)
	{
		Elements infos =doc.getElementById("details").getElementsByTag("tr");
		for(Element e:infos)
		{
			if(e.getElementsByTag("th").html().equalsIgnoreCase(inf))
			{
				return e.getElementsByTag("td").text();
			}
		}
		return "";
	}
	
	public String getPresentation()
	{
		
		return doc.getElementsByClass("element_deco").get(1).text();
	}
	


}
