package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import recherchepack.Reponse;

public class DepartementMath implements Parser{
private Document doc;
	
	
	
	public DepartementMath() {
		super();
		doc=null;
	}
	
	
	public Reponse parser(String url, ArrayList<String> requette) {


		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		reponse.ajouterProprieteValeur("Titre :",getTitreMath());
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("directeur"))
				{
					reponse.ajouterProprieteValeur("Directeur :", getDirecteur());
				}
				else if(requette.get(i).equals("contact"))
				{
					reponse.ajouterProprieteValeur("Contact :",getContact());
				}
				
			}
		}	
		else 
		{
			reponse.ajouterProprieteValeur("directeur :", getDirecteur());
			reponse.ajouterProprieteValeur("Contact :",getContact());
			
		}
		
		
		
				
		
		
		return reponse;
	}
	
	
	public String  getTitreMath()
	{
		
		return  doc.getElementsByTag("title").text().substring(9);
	}
	public String  getDirecteur()
	{
		
	  String s=doc.getElementsByTag("ul").get(6).html();
	  StringBuffer resultat= new StringBuffer();	
	  resultat.append (s.replaceAll ("<[^>]+>",""));
	  String  result= resultat.toString();
	  return result;
		    	
	}
	public String getContact()
	{
		String s=doc.getElementsByTag("ul").get(7).html();
		StringBuffer resultat= new StringBuffer();	
	    resultat.append (s.replaceAll ("<[^>]+>",""));
		String  result= resultat.toString();
		return result;
		
	}
	
	
	



}
