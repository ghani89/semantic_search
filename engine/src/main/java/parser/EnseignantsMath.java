package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import recherchepack.Reponse;

public class EnseignantsMath implements Parser {
private Document doc;
	
	
	
	public EnseignantsMath() {
		super();
		doc=null;
	}
	
	
	public Reponse parser(String url, ArrayList<String> requette) {


		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("personnel") || requette.get(i).equals("membres")|| requette.get(i).equals("enseingants"))
				
					reponse.ajouterProprieteValeur("Enseigants :", getEnseignants());
				
				
			}
		}	
		else 
		{
			reponse.ajouterProprieteValeur("Enseignants :", getEnseignants());
			
		}
      return reponse;
	}

	
public String getEnseignants(){
	String s=doc.getElementsByTag("tbody").text();  
	StringBuffer resultat= new StringBuffer();
	resultat.append (s.replace("Lien",""));
	s=resultat.toString();
	  while(s.contains("Lien")){
	    resultat.append (s.replace("Lien"," "));
	    resultat.append (s.replace("  ","\n\n"));
	    
	  }
	  s=resultat.toString(); 
	  resultat.append(s.replace("Formulaire de contact e-mail", ""));  
	  s=resultat.toString();
	  
     return s;
		
	}
	
}
