package parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import recherchepack.Reponse;

public class ParserFactory {
	
	private HashMap<String,Class> map ;

	public ParserFactory(String nomInstance) throws ClassNotFoundException, ParserConfigurationException, SAXException, IOException {
		super();
		XmlUrlParser xup = new XmlUrlParser("urlparser.xml", nomInstance);
		map = xup.getMap();
		
		
	}
	
	//cette methode instancé les parseurs et les interroge et recupere les reponses dans une collection  
	public ArrayList<Reponse> getResponse(ArrayList<String> request)
	{
		ArrayList<Reponse> reponse = new ArrayList<Reponse>();
		
		for(String url : map.keySet())
		{
			Class c =map.get(url);
			Parser p = null;
			try {
				 p = (Parser) c.newInstance();
			} catch (InstantiationException e) {
				
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				
				e.printStackTrace();
			}
			
			Reponse r = p.parser(url, request);
			if(r!=null)
				reponse.add(r);
			
		}
		
		return reponse;
	}

}
