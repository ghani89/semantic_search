package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recherchepack.Reponse;

public class ModuleParser implements Parser {
	Document doc;
	
	
	

	public ModuleParser() {
		super();
		doc= null;
	}


	public Reponse parser(String url, ArrayList<String> requette) {
		
		
		
		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		Reponse reponse = new Reponse(requette.get(0));
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("titre"))
				{
					reponse.ajouterProprieteValeur("Titre : ",getTitre());
				}
				else if(requette.get(i).equals("discipline"))
				{
					reponse.ajouterProprieteValeur("Discipline : ",getInfGeneral("Discipline(s)"));
				}else if(requette.get(i).equals("grade"))
				{
					reponse.ajouterProprieteValeur("Grade : ",getInfGeneral("Grade"));
					
				}
				else if(requette.get(i).equals("ects"))
				{
					reponse.ajouterProprieteValeur("ECTS : ",getInfGeneral("ECTS"));
					
				}else if(requette.get(i).equals("volume horaire"))
				{
					String v = "total :"+getInfGeneral("Volume horaire total") +"\n CM:"+getInfGeneral("Volume horaire CM")+
							"\n TD"+getInfGeneral("Volume horaire TD");
					
					reponse.ajouterProprieteValeur("Volume horaire : ",v);
					
				}
				
				
			}
			
		}
		else
		{
			
			reponse.ajouterProprieteValeur("Année universitaire ",getAnneeUniv());
			reponse.ajouterProprieteValeur("Titre : ",getTitre());
			reponse.ajouterProprieteValeur("Discipline : ",getInfGeneral("Discipline(s)"));
			reponse.ajouterProprieteValeur("Grade : ",getInfGeneral("Grade"));
			reponse.ajouterProprieteValeur("ECTS : ",getInfGeneral("ECTS"));
			
			
			
			
		}
		
		return reponse;
	}
	
	
	private String getAnneeUniv()
	{
		 return  doc.getElementById("phrase_auto").text();
	}
	private String  getTitre()
	{
		
		return  doc.getElementById("contenu_avec_encadres_avec_nav").getElementsByTag("h1").get(0).text();
	}
	
	private String getInfGeneral(String inf)
	
	{
		Elements infos =doc.getElementById("details").getElementsByTag("tr");
		for(Element e:infos)
		{
			if(e.getElementsByTag("th").html().equalsIgnoreCase(inf))
			{
				return e.getElementsByTag("td").text();
			}
		}
		return "";
	}
	

}
