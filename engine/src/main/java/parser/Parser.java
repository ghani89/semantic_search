package parser;

import java.util.ArrayList;

import recherchepack.Reponse;

public interface Parser {
	
	public Reponse parser(String url , ArrayList<String> requette);

}
