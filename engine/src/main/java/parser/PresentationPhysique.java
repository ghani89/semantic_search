package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recherchepack.Reponse;

public class PresentationPhysique implements Parser{
private Document doc;
	
	
	
	public PresentationPhysique() {
		super();
		doc=null;
	}
	
	
	public Reponse parser(String url, ArrayList<String> requette) {


		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		
		Reponse reponse = new Reponse(requette.get(0));
		
		
		if( requette.size() > 1 )
		{
			for(int i = 1 ; i < requette.size();i++)
			{
				if(requette.get(i).equals("personnel"))
				{
					reponse.ajouterProprieteValeur("personnel :", getPersonnel());
				}
				else if(requette.get(i).equals("laboratoires"))
				{
					reponse.ajouterProprieteValeur("laboratoires :",getLabo());
				}
				
			}
		}	
		else 
		{
			reponse.ajouterProprieteValeur("personnel :", getPersonnel());
			reponse.ajouterProprieteValeur("laboratoires :",getLabo());
			
		}
      return reponse;
	}

	
public String getPersonnel(){
	 String s=doc.getElementsByClass("style2_deco").get(0).html();
     StringBuffer resultat= new StringBuffer();	
     resultat.append (s.replaceAll ("<[^>]+>",""));
     String  result= resultat.toString();
     return result;
		
	}
public String getLabo(){
	 String s=doc.getElementsByClass("style2_deco").get(2).html();
     StringBuffer resultat= new StringBuffer();  
	 resultat.append (s.replaceAll ("<[^>]+>",""));
      String  result= resultat.toString();
	return result;
}	
}
