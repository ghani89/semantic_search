package parser;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import recherchepack.Reponse;




public class EnseignantParser implements Parser {

	Document doc;
	
	
	
	public EnseignantParser() {
		super();
		doc = null;
	}



	public Reponse parser(String url, ArrayList<String> requette) {
		

		try {
			doc = Jsoup.connect(url).timeout(300000).get();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		if(doc == null || requette == null || requette.size() == 0)
			return null;
		Reponse reponse = new Reponse(requette.get(0));
		
		reponse.ajouterProprieteValeur("Nom", getTitre());
		reponse.ajouterProprieteValeur("Statut", getInfGeneral("Statut :"));
		reponse.ajouterProprieteValeur("composante", getInfGeneral("Composante :"));
		reponse.ajouterProprieteValeur("Autre rattachement", getInfGeneral("Autre(s) rattachement(s) :"));
		reponse.ajouterProprieteValeur("Courriel", getInfGeneral("Courriel :"));
		reponse.ajouterProprieteValeur("Adresse", getInfGeneral("Adresse :"));
		reponse.ajouterProprieteValeur("Bureau", getInfGeneral("Bureau :"));
		reponse.ajouterProprieteValeur("Téléphone", getInfGeneral("Téléphone : "));
		reponse.ajouterProprieteValeur("Fax", getInfGeneral("Fax :"));
		reponse.ajouterProprieteValeur("Photo", getPhoto());
		System.out.println(getPhoto());
		return reponse;
	}
	
	private String  getTitre()
	{
		
		return  doc.getElementById("contenu_avec_encadres_avec_nav").getElementsByTag("h1").get(0).text();
	}
	
	private String getInfGeneral(String inf)
	
	{
		Elements infos =doc.getElementById("details").getElementsByTag("tr");
		for(Element e:infos)
		{
			if(e.getElementsByTag("th").html().equalsIgnoreCase(inf))
			{
				return e.getElementsByTag("td").text();
			}
		}
		return "";
	}
	
	private String getPhoto()
	{
		String url;
		
		Elements img = doc.getElementById("photo_encadre").getElementsByTag("img");
		
		String imgSrc = img.attr("src");
		url="http://www.uvsq.fr"+imgSrc;
		
		return url;
	}
	
	
	

}
